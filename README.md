
# "Front-End" Car Management Dashboard (Chapter 5 - Main Challenge)

A simple car management dashboard where you can do basic CRUD operation with it.

## Description

This is the final project for me to pass this Chapter 5 and continue to the next chapter, in this chapter we were asked to create a simple car management information system where we can perform basic CRUD operations on the car data there. This project has used a database with PostgreSQL as the DBMS. Using Node.js to create HTTP Server and Express comes to play as the framework, renders HTML elements using View Engine EJS, and uses the Sequelize module as the ORM.

## Tech Stack

* Client :
    * SB Admin 2 (Admin Template)
    * EJS 3.1.7 (View Engine)
    
* Server :
    * Node.js 16.14 (Runtime Environment)
    * Express 4.17.3 (HTTP Server)
    * EJS 3.1.7 (View Engine)
    * Sequelize 6.19.0 (ORM)
    * PostgreSQL 14.2 (DBMS)
    * Axios 0.26.1 (HTTP Request)
    * Multer 1.4.4 (Uploading Files)

## Prerequisite

Things you need to install on your machine in order for the website to works, so make sure you've installed it before proceed to the next step.
* [Backend Repository](https://gitlab.com/frizaadityafrinison/car-management-dashboard)

## Installation

Clone this repository

```bash
cd same_directory_with_backend/
git clone https://gitlab.com/frizaadityafrinison/car-management-dashboard-front-end
cd car-management-dashboard-front-end
```

Download all the package and it's dependencies
```bash
npm install 
```

Change the config.js
```bash
cd config/config.json
cat config.json
```

## Running Server
To run the server, run the following command 
**(Again make sure you run the back-end first before proceed to the next step)**

```bash
  npm run start
```

## Entity Relationship Diagram

![ERD](/uploads/fe5ed56735e25dacbd63b63affcf23ea/ERD.png)

## Walkthrough (It Works On My Machine®)

Once you are done open your browser and visit http://localhost:8000 (because this server is running on port 8000)
to test the website, here you can do some basic CRUD operations. To turn off the server simply by press **Ctrl + C** in your terminal where the server is running.

**Home**
![Home](/uploads/d5f78602349585dc26edb470a4167bb7/Home.png)

**Add Car**
![Add_Car](/uploads/6921f5ca05f59cd1d07c2f7e6d02e9cb/Add_Car.png)

**Edit Car**
![Edit_Car](/uploads/9c824e66ca181aee7a4be965859b1491/Edit_Car.png)

**Delete Car**
![Delete_Car](/uploads/e10b4a0f06521edaa8b417df14fb83f2/Delete_Car.png)
