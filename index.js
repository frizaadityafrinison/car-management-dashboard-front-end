const { default: axios } = require('axios');
const express = require('express');
const app = express();
const port = 8000;
const multer = require('multer');
const { Car } = require('./models');

app.use(express.static(`public`));
app.use(express.urlencoded({extended:true}));
app.set(`view engine`, 'ejs');
app.use(express.json());

const storage = multer.diskStorage({
    destination: function(req, file, cb){
        cb(null, './public/img/uploaded');
    },
    filename: function(req, file, cb){
        console.log(file);
        cb(null, file.originalname);
    }
})

// ------------------------------------------------------------------------------------------------------------------LIST ALL CARS
app.get('/', (req, res) => {
    Car.findAll()
    .then(data => {
        res.render('index', {
            cars: data
        });
    })
});
// ------------------------------------------------------------------------------------------------------------------DELETE A CAR
app.get('/delete/:id', (req, res) => {
    const { id } = req.params;
    axios.delete(`http://localhost:8080/${id}`);
    setTimeout(() => {
        res.redirect('/');
    }, 500);
});
// ------------------------------------------------------------------------------------------------------------------CREATE A CAR
app.post('/', (req, res) => {
    let upload = multer({ storage:storage }).single('foto');

    upload(req, res, function(err) {
        console.log(req.file);
        Car.create({
            nama: req.body.nama,
            harga: req.body.harga,
            ukuran: req.body.ukuran,
            foto: req.file ? req.file.originalname : ""
        });

        setTimeout(() => {
            res.redirect('/');
        }, 400);
    })
})

app.get('/addcar', (req, res) => {
    res.render('addcar', {
        action: "/"
    });
})

app.post('/', (req, res) => {
    axios.post(`http://localhost:8000/`);
    setTimeout(() => {
        res.redirect('/');
    }, 250);
})
// ------------------------------------------------------------------------------------------------------------------UPDATE A CAR
app.get("/:id", (req, res) => {
    const id = parseInt(req.params.id);
    axios.get(`http://localhost:8080/${id}`).then(cars=>{
      res.render("editcar", {
        dataPilih : cars.data[0],
      })
    })
})
  
app.post("/:id", (req, res) => {
    let upload = multer({storage:storage}).single('foto')
    upload(req, res, function(err) {
        const search = Car.findOne({
            where: {id:req.params.id}
        }).then(cars => {
            Car.update({
                nama: req.body.nama,
                harga: req.body.harga,
                ukuran: req.body.ukuran,
                foto: req.file ? req.file.originalname : ""
            }, {
                where: {id: req.params.id}
            })
        })
    })

    setTimeout(() => {
        res.redirect('/');
    }, 250);
})

app.listen(port, () => {
    console.log(`http://localhost:${port}`);
})